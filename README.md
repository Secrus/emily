# Emily

**Emily** is made as a way for me to track and manage my book collection. It also serves me as playground to test new tools, libraries and approaches to building projects.

## TODO
- add custom user model
- add CI config
- add `pytest` config
- add `tox` config
- add `pre-commit` config
- add `black` config (and to `pre-commit`)
- add `poe` task runner 
- add Dockerfile and `docker-compose`
- add code (*duh!*)


